<?php

namespace Tests\Feature;

use App\Entities\AttendanceHandler;
use App\Jobs\ReadAttendanceFile;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SaveAttendanceDataTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_write_and_read_data()
    {
        ReadAttendanceFile::dispatch("foris_test_data", "txt");
        $response = $this->get('api/attendance/getReport');
        $response->assertJsonPath('message.0.total_minutes', 142);
        $response->assertJsonPath('message.0.total_days', 2);
        $response->assertJsonPath('message.1.total_minutes', 104);
        $response->assertJsonPath('message.1.total_days', 1);
        $response->assertJsonPath('message.2.total_minutes', 0);
        $response->assertJsonPath('message.2.total_days', 0);

    }

}
