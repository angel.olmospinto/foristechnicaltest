
## Sobre el proyecto

Implementación de una solución al desafío tecnico planteado por foris
La siguiente solución fue desarrolalda con el lenguaje PHP V8.1 bajo el framework Laravel V9

### Problema palteado


Generar un reporte que liste cada estudiante con el total de minutos registrados y la cantidad de días que asistió a la universidad. Ordenando el resultado por la cantidad de minutos de mayor a menor.
Los datos para generar dicho reporte deben ser obtenidos desde un archivo txt que incluye comandso de registro de estudiantes y resgistro de asistencia

Ejemplo de entrada:

```
Student Marco
Student David
Student Fran
Presence Marco 1 09:02 10:17 R100
Presence Marco 3 10:58 12:05 R205
Presence David 5 14:02 15:46 F505
```

Ejemplo de salida:

```
Marco: 142 minutes in 2 days
David: 104 minutes in 1 day
Fran: 0 minutes
```

## Descripción de la solución

La solución planteada consiste una API REST que disponibiliza los siguientes endpoits


### URL BASE
```
http://api.local/api/

```

### Carga de datos

Parámetros para la solicitud
`file_name`

Requiere que previamente exista el achivo cargado en la ruta ./foristechnicaltest/storage/app/public
Se simula una carga previa del mismo mediante FTP, y estructura de manera que pueda escalar a la lectura de datos desde otras fuentes.
La lectura y procesamiento de los datos se realiza en segundo plano.

Solicitud:

attendance/loadData/`file_name`

```
http://api.local/api/attendance/loadData/file_name.txt

```

Respuesta:

```
{
    "internal_code": 200,
    "message": "Lectura en curso"
}
```


### Solicitud de reporte

Solicitud:

/attendance/getReport

```
http://api.local/api/attendance/getReport

```


Respuesta
```
{
    "internal_code": 200,
    "message": {
        {
            "name": "Marco",
            "detail": {
                "minutes": [
                    75,
                    67
                ],
                "days": [
                    "1",
                    "3"
                ],
                "classrooms": [
                    "R100",
                    "R205"
                ]
            },
            "total_minutes": 142,
            "total_days": 2
        },
    }
}
```

### Correr test

php artisan test


### Levantar servidor web local

php artisan serve --host=IP --port=80
