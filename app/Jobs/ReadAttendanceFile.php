<?php

namespace App\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class ReadAttendanceFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $filename;
    protected $type;

    //Command List
    protected $STUDENT = "STUDENT";
    protected $PRESENCE = "PRESENCE";

    public function __construct($filename, $type)
    {
        $this->filename = $filename;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = explode(PHP_EOL, Storage::disk('public')->get("{$this->filename}.{$this->type}"));
        
        $presence_data = collect();

        foreach($data as $line){
            $line_data = explode(" ", $line);
            if(isset($line_data[0])){
                $command = strtoupper($line_data[0]);
                switch($command){  
                    case $this->STUDENT:
                                if($presence_data->search(strtoupper($line_data[1])) === false){
                                    //Nuevo estudiante por registrar
                                    $presence_data->put(
                                                        strtoupper($line_data[1]) ,
                                                            (object) [
                                                                    'name' => $line_data[1],    
                                                                    'detail' => (object) [
                                                                        "minutes" => collect(),
                                                                        "days" => collect(),
                                                                        "classrooms" => collect(),
                                                                    ],
                                                                    'total_minutes' => 0,
                                                                    'total_days' => 0
                                                            ]
                                                        );
                                }
                        break;
                    case $this->PRESENCE:
                                if(isset($presence_data[strtoupper($line_data[1])])){
                                    //Estudiante registrado
                                    $student = $presence_data[strtoupper($line_data[1])];
                                    $ingreso = Carbon::parse($line_data[3]);
                                    $salida = Carbon::parse($line_data[4]);
                                    $presence_minutes = $ingreso->diffInMinutes($salida);
                                    if($presence_minutes > 5){
                                        $student->detail->minutes->push($presence_minutes);
                                        $student->detail->days->push($line_data[2]);
                                        $student->detail->classrooms->push($line_data[5]);
                                        $student->total_minutes = $student->detail->minutes->sum();
                                        $student->total_days = $student->detail->days->unique()->count();
                                    }
                                }
                        break;
                    default:
                            //LOG Comando no aceptado {$command} from File {$this->filename}
                        break;
                }
            }
        }
        Storage::disk('public')->put("database.json", $presence_data->values()->sortByDesc('total_minutes')->values()->toJson());
    }
}
