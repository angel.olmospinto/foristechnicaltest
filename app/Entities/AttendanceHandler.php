<?php

namespace App\Entities;

use App\Jobs\ReadAttendanceFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AttendanceHandler
{
    protected $filename;
    protected $type;

    public function __construct($file = null)
    {
        $data = explode(".", $file);
        $this->filename = $data[0] ?? null;
        $this->type = $data[1] ?? null;    
        
    }

    public function saveData()
    {

        if(!isset($this->filename) || !isset($this->type)){
            return response()->json(['internal_code' => 404, 'message' =>  "No se define nombre o extensión del archivo" ], 404);
        }

        //TO-DO pendiente de configuración para gestión de colas
        //Despacha un Job para lectura del archivo en segundo plano
        ReadAttendanceFile::dispatchAfterResponse($this->filename, $this->type);
        return response()->json(['internal_code' => 200, 'message' => "Lectura en curso" ], 200);
    }

    public function readData()
    {
        $data = json_decode(Storage::disk('public')->get("database.json"));
        return response()->json(['internal_code' => 200, 'message' =>  $data ], 200);
    }
    
}