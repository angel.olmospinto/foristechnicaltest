<?php

namespace App\Http\Controllers;

use App\Entities\AttendanceHandler;
use GuzzleHttp\Psr7\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redis;

class AttendanceController extends Controller
{
    
    public function loadDataFromFileName($filename){
        return (new AttendanceHandler($filename))->saveData();
    }

    public function readData(){
        return (new AttendanceHandler())->readData();
    }

}
