<?php

use App\Http\Controllers\AttendanceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'attendance'], function ($app) {
    $app->get('/loadData/{filename}', [AttendanceController::class, 'loadDataFromFileName']);
    $app->get('/getReport', [AttendanceController::class, 'readData']);
});
